class IsAdmin(BasePermission):
    def has_permission(self, request, *args):
        if not is_authenticated(request.user):
            return

        if request.user.type != 3:
            return

        return True
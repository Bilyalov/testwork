from rest_framework.routers import DefaultRouter
from Work.views import TaskViewSet, PersonViewSet

router = DefaultRouter()
router.register('tasks', TaskViewSet, base_name='tasks')
router.register('persons', PersonViewSet, base_name='persons')

urlpatterns = router.urls

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import User, PermissionsMixin
from django.db import models


class City(models.Model):
    name = models.CharField('Название города', max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Position(models.Model):
    name = models.CharField('Название должности', max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'


class PersonManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            return ValueError("Users must have a email")

        user = self.model(
            email=email
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class Person(AbstractBaseUser):
    fullname = models.CharField('Имя', max_length=60)
    position = models.ForeignKey('Work.Position', verbose_name='Должность', null=True, on_delete=models.SET_NULL)
    city = models.ForeignKey('Work.City', verbose_name='Город', null=True, on_delete=models.SET_NULL)
    email = models.EmailField('Почта', unique=True)
    is_admin = models.BooleanField(default=False)
    objects = PersonManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return str(self.pk) + ' ' + str(self.fullname)

    def has_perms(self, perm, obj=None):
        return True

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        ordering = ['fullname']
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Task(models.Model):
    name = models.CharField('Название задачи', max_length=100)
    description = models.TextField('Описание задачи')
    assigned_users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name='Назначенные пользователи', blank=True)
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'



from django.contrib import admin
from django.contrib.auth.models import Group

from Work.models import Task, Person, City, Position


class TaskAdmin(admin.ModelAdmin):
    list_display = ['name', 'description']


admin.site.unregister(Group)

admin.site.register(Person)
admin.site.register(Task, TaskAdmin)
admin.site.register(City)
admin.site.register(Position)
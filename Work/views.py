from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAdminUser, IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_201_CREATED
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from Work.filters import TaskFilter, PersonFilter
from Work.mixins import PersonCreateModelMixin
from Work.models import Task, Person
from Work.serializers import TaskFullSerializer, TaskAssignSerializer, TaskReadSerializer, \
    PersonWriteSerializer, PersonReadSerializer


class TaskViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class = TaskFullSerializer
    http_method_names = ['get', 'post', 'delete', 'put', 'patch']  # disable 'head', 'options', 'trace' methods
    filter_backends = [DjangoFilterBackend]
    filter_class = TaskFilter

    # Считаю, что лучше разделить логику, даже если она является практически одинаковой,
    # чтобы в будущем было проще работать если появятся дополнения в одном из действий

    @action(detail=True, methods=['post'], permission_classes=[IsAdminUser])
    def assign(self, request, pk=None, *args, **kwargs):
        serializer = TaskAssignSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # data = serializer.validated_data
        # Можно было бы вынести serializer.validated_data в переменную для красоты,
        # но я считаю, что только для одного действия в данном коде лучше взять инфу с сериалайзера - оптимизация =/

        task = get_object_or_404(Task, pk=pk)
        task.assigned_users.add(serializer.validated_data.get('person'))

        serializer = TaskFullSerializer(task)

        return Response(serializer.data, HTTP_200_OK)

    @action(detail=True, methods=['post'], permission_classes=[IsAdminUser])
    def deassign(self, request, pk=None, *args, **kwargs):
        serializer = TaskAssignSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        task = get_object_or_404(Task, pk=pk)
        task.assigned_users.remove(serializer.validated_data.get('person'))

        serializer = TaskFullSerializer(task)

        return Response(serializer.data, HTTP_200_OK)

    @action(detail=True, methods=['get'], permission_classes=[IsAdminUser])
    def users(self, request, pk=None, *args, **kwargs):
        task = get_object_or_404(Task, pk=pk)
        users = task.assigned_users.all()

        serializer = PersonReadSerializer(users, many=True)

        return Response(serializer.data, HTTP_200_OK)


class PersonViewSet(PersonCreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    queryset = Person.objects.all()
    http_method_names = ['get', 'post', 'delete', 'put', 'patch']
    filter_backends = [DjangoFilterBackend]
    filter_class = PersonFilter
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method in SAFE_METHODS:
            return PersonReadSerializer
        return PersonWriteSerializer

    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated])
    def tasks(self, request, pk=None, *args, **kwargs):
        tasks = Task.objects.all().filter(assigned_users__in=[pk])

        if tasks.__len__() == 0:
            return Response([], HTTP_404_NOT_FOUND)

        serializer = TaskReadSerializer(tasks, many=True)
        return Response(serializer.data, HTTP_200_OK)

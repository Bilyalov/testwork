from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer
from Work.models import Task, Person, Position, City


class PersonPositionSerializer(ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'


class PersonCitySerializer(ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class PersonReadSerializer(ModelSerializer):
    # Либо можно использовать вложенные сериалайзеры
    position = PersonPositionSerializer(read_only=True)
    city = PersonCitySerializer(read_only=True)

    class Meta:
        model = Person
        exclude = ['password', 'last_login', 'is_admin']
        # depth = 1  Либо просто указать глубину вложенности, но не во всех случаях


class PersonWriteSerializer(ModelSerializer):
    class Meta:
        model = Person
        exclude = ['password', 'last_login', 'is_admin']


class TaskFullSerializer(ModelSerializer):
    assigned_users = PersonWriteSerializer(many=True, read_only=True)  # либо можно убрать вложенный сериалайзер, зависит от логики на фронте

    class Meta:
        model = Task
        fields = '__all__'


class TaskReadSerializer(ModelSerializer):
    class Meta:
        model = Task
        exclude = ['assigned_users']


class TaskAssignSerializer(Serializer):
    person = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), required=True)

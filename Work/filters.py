import django_filters
from Work.models import Person, Task


class PositionCommaSeperatedFilter(django_filters.NumberFilter, django_filters.BaseInFilter):
    pass


class PersonFilter(django_filters.rest_framework.FilterSet):
    position = PositionCommaSeperatedFilter(field_name='position', lookup_expr='in')

    class Meta:
        model = Person
        fields = ['position']


class TaskFilter(django_filters.rest_framework.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Task
        fields = ['name']


